package com.siddisoft.attendenceapp.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.drawerlayout.widget.DrawerLayout;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import com.siddisoft.attendenceapp.R;
import com.siddisoft.attendenceapp.fragments.HomeFragment;
import com.siddisoft.attendenceapp.helpers.Helper;

public class DashBoardActivity extends AppCompatActivity implements View.OnClickListener {

    private Activity activity;
    private Context context;

    private DrawerLayout drawer;
    private HomeFragment homeFragment;
    private Toolbar toolbar;
    private ImageView drawerMenuImageView, drawerNotificationImageView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dash_board);

        activity = DashBoardActivity.this;
        context = DashBoardActivity.this;

        drawer = findViewById(R.id.drawer_layout);
        toolbar = findViewById(R.id.toolbar);
        drawerMenuImageView = toolbar.findViewById(R.id.drawer_menu_iv);
        drawerNotificationImageView = toolbar.findViewById(R.id.drawer_notification_iv);

        homeFragment = HomeFragment.newInstance();
        Helper.replaceFragment(homeFragment, activity);

        drawerMenuImageView.setOnClickListener(this);
        drawerNotificationImageView.setOnClickListener(this);

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.drawer_menu_iv:
                drawer.openDrawer(Gravity.LEFT);
                break;
            case R.id.drawer_notification_iv:
                Toast.makeText(context, "Notifications Clicked!", Toast.LENGTH_LONG).show();
                break;
        }
    }
}
