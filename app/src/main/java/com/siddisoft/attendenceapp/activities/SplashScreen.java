package com.siddisoft.attendenceapp.activities;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;

import com.mikhaellopez.circularimageview.CircularImageView;
import com.siddisoft.attendenceapp.R;

public class SplashScreen extends AppCompatActivity {

    private Activity activity;
    private Context context;
    private CircularImageView logoCircularImagrView;
    private Animation animZoomin;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);

        activity = SplashScreen.this;
        context = SplashScreen.this;

        logoCircularImagrView = findViewById(R.id.logo_circular_iv);

        animZoomin = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.zoom_in);
        logoCircularImagrView.startAnimation(animZoomin);

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                startActivity(new Intent(context, LoginActivity.class));
                finish();
            }
        }, 3000);

    }
}
