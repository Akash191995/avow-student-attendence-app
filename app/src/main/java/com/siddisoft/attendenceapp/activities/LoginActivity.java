package com.siddisoft.attendenceapp.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.siddisoft.attendenceapp.R;

public class LoginActivity extends AppCompatActivity implements View.OnClickListener {

    private Activity activity;
    private Context context;
    private ConstraintLayout loginTextView, signUpTextView;
    private TextView forgotPasswordTextView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        activity = LoginActivity.this;
        context = LoginActivity.this;

        loginTextView = findViewById(R.id.login_cl);
        signUpTextView = findViewById(R.id.signup_cl);
        forgotPasswordTextView = findViewById(R.id.forgot_password_tv);

        loginTextView.setOnClickListener(this);
        signUpTextView.setOnClickListener(this);
        forgotPasswordTextView.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.login_cl:
                startActivity(new Intent(context, DashBoardActivity.class));
                finish();
                break;
            case R.id.signup_cl:
                startActivity(new Intent(context, DashBoardActivity.class));
                break;
            case R.id.forgot_password_tv:
                Toast.makeText(context, "clicked!", Toast.LENGTH_LONG).show();
                break;
        }
    }
}
